import Task from './../models/Task.js';
import { getListTask, deleteTask, addTask, updateTask, getTaskById } from './../models/TaskService.js';



const getAllTasks = () => {
    document.getElementById('loader').style.display = 'block';
    getListTask().then((res) => {
        document.getElementById('loader').style.display = 'none';
        renderHTML(res.data);
    }).catch((err) => {
        console.log(err);
    })
}

const renderHTML = (arr) => {
    let contentHTMLTodo = "";
    let contentHTMLComplete = "";
    arr.forEach((item) => {
        if (item.status === "toDo") {
            contentHTMLTodo += `
        <li>
            <span>${item.textTask}</span>
            <div class="buttons">
                <button class="remove">
                    <i class="fa fa-trash-alt" onclick="removeTask('${item.id}')"></i>
                </button>
                <button class="complete" onclick="updateStatus('${item.id}')">
                    <i class="far fa-check-circle"></i>
                    <i class="fas fa-check-circle"></i>
                </button>
            </div>
        </li>
        `
        } else if (item.status === "complete") {
            contentHTMLComplete += `
            <li>
                <span class="greenTextAndCheck">${item.textTask}</span>
                <div class="buttons">
                <button class="remove" onclick="removeTask('${item.id}')">
                    <i class="fa fa-trash-alt"></i>
                </button>
                <button class="complete"  onclick="updateStatus('${item.id}')">
                    <i class="far fa-check-circle greenTextAndCheck"></i>
                    <i class="fas fa-check-circle greenTextAndCheck"></i>
                </button>
                </div>
            </li>
            `
        }
    })

    document.getElementById("todo").innerHTML = contentHTMLTodo;
    document.getElementById("completed").innerHTML = contentHTMLComplete;
}

const removeTask = (id) => {
    deleteTask(id).then((res) => {
        alert("Xoá task " + id + " thành công");
        console.log(res.status);
        getAllTasks();
    }).catch((err) => {
        console.log(err);
    })
}
document.getElementById("addItem").addEventListener("click", () => {

    let task = document.getElementById("newTask").value;
    if (!task) {
        document.getElementById("notiInput").style.display = "block";
        return;
    } else {
        document.getElementById("notiInput").style.display = "none";
    }
    let newTask = new Task("", task, "toDo", "Tri Task");
    addTask(newTask).then((res) => {
        console.log(res.data);
        alert("Thêm task thành công");
        getAllTasks();
    }).catch((err) => {
        console.log(err);
    })
})

const updateStatus = async(id) => {
    let as = await getTaskById(id).then((res) => {
        return res.data
    }).catch((err) => {
        console.log(err);
    });
    let newStatus = "";
    if (as.status === "toDo") {
        newStatus = "complete";
    } else if (as.status === "complete") {
        newStatus = "toDo";
    }
    let upTask = new Task(id, as.textTask, newStatus, as.taskName);
    updateTask(upTask).then((res) => {
        getAllTasks();
        alert("Chuyển trang thái thành công " + res.status);
    }).catch((err) => {
        console.log("Lỗi Update task " + err);
    });
    // getTaskById(id)
    //     .then((res) => {
    //         let newStatus = "";
    //         if (res.data.status === "toDo") {
    //             newStatus = "complete";
    //         } else if (res.data.status === "complete") {
    //             newStatus = "toDo";
    //         }
    //         console.log(newStatus);
    //         let upTask = new Task(id, res.textTask, newStatus, res.taskName);
    //         updateTask(upTask).then((res) => {
    //             getAllTasks();
    //             alert("Chuyển trang thái thành cônng");
    //             console.log(res.data);
    //         }).catch((err) => {
    //             console.log("Lỗi Update task");
    //         });
    //     })
    //     .catch((err) => {
    //         console.log(err);
    //     })
}
window.updateStatus = updateStatus;
window.removeTask = removeTask;
getAllTasks();