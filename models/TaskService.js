const getListTask = () => {
    return axios({
        url: 'https://6041bf467f50e000173aaf76.mockapi.io/todolist',
        method: 'GET'
    })
}

const deleteTask = (id) => {
    return axios({
        url: 'https://6041bf467f50e000173aaf76.mockapi.io/todolist/' + id,
        method: 'DELETE'
    })
}
const addTask = (data) => {
    return axios({
        url: 'https://6041bf467f50e000173aaf76.mockapi.io/todolist/',
        method: 'POST',
        data: data
    })
}
const getTaskById = (id) => {
    return axios({
        url: 'https://6041bf467f50e000173aaf76.mockapi.io/todolist/' + id,
        method: 'GET'
    })
}

const updateTask = (data) => {
    return axios({
        url: 'https://6041bf467f50e000173aaf76.mockapi.io/todolist/' + data.id,
        method: 'PUT',
        data: data
    })
}


export { getListTask, deleteTask, addTask, updateTask, getTaskById }