export default class Task {
    constructor(id, textTask, status, taskName) {
        this.id = id;
        this.textTask = textTask;
        this.status = status;
        this.taskName = taskName;
    }
}